module ParseWhile where

import System.IO
import Control.Monad
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language

import qualified Text.ParserCombinators.Parsec.Token as Token

-- The grammar
--
--  a -> x | n | -a | a opa a
--  b -> true | false | not b | b opb b | a opr a
--  opa -> + | - | * | /
--  opb -> and | or
--  opr -> > | <
--
--  S -> x := a
--     | skip
--     | S1; S2
--     | (S)
--     | if b then S1 else S2 | while b do S
--
-- Ref: Parsing a simple imperative language Haskell

-- boolean expressions
data BExpr = BoolConst Bool
           | Not BExpr
           | BBinary BBinOp BExpr BExpr
           | RBinary RBinOp AExpr AExpr
             deriving (Show)

-- binary boolean operators
data BBinOp = And | Or deriving (Show)

-- binary relational operators
data RBinOp = Greater | Less deriving (Show)

-- arithmetic expressions
data AExpr = Var String
           | IntConst Integer
           | Neg AExpr
           | ABinary ABinOp AExpr AExpr
             deriving (Show)

-- arithmetic binary operators
data ABinOp = Add | Sub | Mul | Div
              deriving (Show)

-- statement
data Stmt = Assign String AExpr
          | Skip
          | Seq [Stmt]
          | If BExpr Stmt Stmt
          | While BExpr Stmt 
            deriving (Show)

-- LExer
-- 
-- 1. Create a language definition
languageDef = 
   emptyDef { Token.commentStart    = "/*"
            , Token.commentEnd      = "*/"
            , Token.commentLine     = "//"
            , Token.identStart      = letter
            , Token.identLetter     = alphaNum
            , Token.reservedNames   = [    "if",  "then", "else",
                                        "while",    "do",
                                         "skip",
                                         "true", "false",
                                          "not",   "and", "or" 
                                      ]
            , Token.reservedOpNames = ["+","-","*","/",
                                       "<",">",
                                       "and","or","not"
                                      ]
            }

lexer      = Token.makeTokenParser languageDef

-- lexemes?
identifier = Token.identifier lexer -- parses identifier
reserved   = Token.reserved   lexer -- parses keyword
reservedOp = Token.reservedOp lexer -- parses operator
parens     = Token.parens     lexer -- _parens p_ 
                                    -- handles parantheses
                                    -- and parses one p
integer    = Token.integer    lexer -- parsers an integer
semi       = Token.semi       lexer -- parses a semicolon
whiteSpace = Token.whiteSpace lexer -- parses whitespaces

-- parser
-- the language is just a statement
whileParser :: Parser Stmt
whileParser = whiteSpace >> statement -- remove initial whitespaces
                                      -- and parse a stmt

-- statement can be a sequence of statements (atleast one)
-- also, can be surrounded by parentheses
statement :: Parser Stmt
statement =   parens statement
          <|> sequenceStmt

sequenceStmt = 
    do list <- (sepBy1 statementSingle semi)
       return $ if length list == 1 
                then head list 
                else  Seq list

-- parse a single statement (assign, skip, if, while)
statementSingle :: Parser Stmt
statementSingle =   ifStmt
                <|> whileStmt
                <|> skipStmt
                <|> assignStmt

-- parse an if block
ifStmt :: Parser Stmt
ifStmt = 
    do reserved "if"
       condn <- bExpression
       reserved "then"
       stmt1 <- statement
       reserved "else"
       stmt2 <- statement
       return $ If condn stmt1 stmt2

-- parse a while block
whileStmt :: Parser Stmt
whileStmt = 
     do reserved "while"
        condn <- bExpression
        reserved "do"
        stmt  <- statement
        return $ While condn stmt

-- parses an assign statement
assignStmt :: Parser Stmt
assignStmt =
     do var <- identifier
        reservedOp ":="
        expr <- aExpression
        return $ Assign var expr

-- parses skip
skipStmt :: Parser Stmt
skipStmt = reserved "skip" >> return Skip

-- expressions
-- AExpr
aExpression :: Parser AExpr
aExpression = buildExpressionParser aOperators aTerm

-- BExpr
bExpression :: Parser BExpr
bExpression = buildExpressionParser bOperators bTerm

-- defined operators used with precedence (non-increasing)
aOperators = [ [Prefix (reservedOp "-" >> return (Neg             )) ]
             , [Infix  (reservedOp "*" >> return (ABinary Mul)) AssocLeft,
                Infix  (reservedOp "/" >> return (ABinary Div)) AssocLeft]
             , [Infix  (reservedOp "+" >> return (ABinary Add)) AssocLeft,
                Infix  (reservedOp "-" >> return (ABinary Sub)) AssocLeft]
             ]

bOperators = [ [Prefix (reservedOp "not" >> return (Not         )) ]
             , [Infix  (reservedOp "and" >> return (BBinary And )) AssocLeft,
                Infix  (reservedOp "or"  >> return (BBinary Or  )) AssocLeft]
             ]

aTerm =  parens aExpression
     <|> liftM Var identifier
     <|> liftM IntConst integer

bTerm =  parens bExpression
     <|> (reserved "true"  >> return (BoolConst True))
     <|> (reserved "false" >> return (BoolConst False))
     <|> rExpression

rExpression =
     do a1 <- aExpression
        op <- relation
        a2 <- aExpression
        return $ RBinary op a1 a2

relation =  (reservedOp ">" >> return Greater)
        <|> (reservedOp "<" >> return Less)

parseString :: String -> Stmt
parseString str = 
           case parse whileParser "" str of
                Left e  -> error $ show e
                Right r -> r

parseFile :: String -> IO Stmt
parseFile file =
       do program <- readFile file
          case parse whileParser "" program of
               Left e  -> print e >> fail "parse error"
               Right r -> return r
