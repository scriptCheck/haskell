import System.Directory (getCurrentDirectory)
import System.FilePath.Find
import System.IO

entry :: IO FilePath
entry = getCurrentDirectory

found :: FilePath -> IO [FilePath]
found = find always (extension ~~? ".hs" ||? extension ~~? ".lhs")

-- main :: IO ()
out = getCurrentDirectory 
          >>= find always 
                   (extension ~~? ".hs" ||? extension ~~? ".lhs")

main = out >>= print
