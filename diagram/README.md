getting started with using Diagrams

Diagrams is an EDSL for Haskell that can be used for declarative vector graphic and animations

The quick start tutorial:
 diagrams.github.io/doc/quickstart.html

##Installation
 The installation is in a sandbox
 Installed by the following command
    cabal update
    cabal sandbox init
    cabal install diagrams
 
 Usage:
 1. Running a .hs file in ghc with diagram module
    cabal exec -- ghc --make MyDiagram.hs
 2. Load a new shell with all the diagrams package available
    cabal exec bash
