> {-# LANGUAGE NoMonomorphismRestriction #-}
> {-# LANGUAGE FlexibleContexts #-}
> {-# LANGUAGE TypeFamilies #-}
>
> -- First statement turns off Monomorphism Restriction (Dreaded), to avoid confusing errors for now :')
> -- Other two are required often by diagrams in general
>
> import Diagrams.Prelude
> import Diagrams.Backend.SVG.CmdLine
>
> myCircle :: Diagram B
> myCircle = circle 1
>
> main = mainWith myCircle
