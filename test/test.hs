module Bintree where 
data BinTree a = NullTree
               | Node (BinTree a) a (BinTree a)

null :: BinTree a -> Bool
null NullTree = True
null _        = False

empty :: BinTree a
empty = NullTree

singleton :: a -> BinTree a 
singleton x = Node NullTree x NullTree

-- Inorder Traversal of a binary tree
inorder :: BinTree a -> [a]
inorder NullTree = []
inorder (Node lt a rt) = inorder lt ++ (a : inorder rt)

{-- 
 anticlockwise rotation of a tree
       a                   b
      / \                 / \
    T1   b     -->       a   T3
        / \             / \   
       T2 T3           T1  T2

--}
anticlockwise :: BinTree a -> BinTree a
anticlockwise (Node t1 a (Node t2 b t3)) = Node (Node t1 a t2) b t3
anticlockwise x = x

