
qSort []     = []
qSort (x:xs) = qSort smaller ++ [x] ++ qSort larger
               where
                     smaller = [a | a <- xs, a <= x]
                     larger  = [b | b <- xs, b > x]

-- what if we use < instead of <= ?
qsort_special []     = []
qsort_special (x:xs) = qsort_special small ++ [x] ++ qsort_special large
                       where
                             small = [a | a <- xs, a < x]
                             large = [b | b <- xs, b > x]


