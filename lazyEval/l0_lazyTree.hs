
data Tree a = Null
            | Node (Tree a) a (Tree a)

maxT :: Tree Int -> Int
maxT Null = 0
maxT (Node lt x rt) = max [maxT lt, x, maxT rt]

withShape :: Tree a -> b -> Tree b
withShape Null           y = Null
withShape (Node lt x rt) y = Node ltp y rtp
                             where
                                  ltp = withShape lt y
                                  rtp = withShape rt y

-- doing it in one go
helper :: Tree Int -> b -> (Int,Tree b)
helper Null           _ = (0                , Null)
helper (Node lt x rt) y = (max [lt1, x, rt1], Node lt2 y rt2)
       where
            (lt1, lt2) = helper lt y
            (rt1, rt2) = helper rt y

-- lazy evaluation !
maxWithShape :: Tree Int -> Tree Int
maxWithShape t = tp
             where 
                  (m, tp) = helper tp m


