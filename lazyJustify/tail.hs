-- Catching the tail 

rightJustify :: [String] -> [String]
length :: [a] -> Int
replicate :: a-> Int -> [a]
justify :: String -> Int -> String

myMap :: (a->b)->[a]->[b]
myMap f [] = []
myMap f (x:xs) = f x : myMap f xs
 
foldl :: (a->b->a)->a->[b]->a
foldl f x [] = x
foldl f x (y:ys) = (f x y) : (foldl f x ys)

mapAccumulate :: (acc -> a -> (b,acc)) -> acc -> [a] -> ([b],acc)
mapAccumulate f x [] = ([],x)
mapAccumulate f x (y:ys) = (b1:b2,acc2)
                           where 
                                b1, acc1 = f x y
                                b2, acc2 = mapAccumulate f acc1 ys


