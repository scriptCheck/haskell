import Data.Bool

filterL :: (a -> Bool) -> [a] -> [a]
filterL f [] = []
filterL f (x:xs) | f x       = x : filterL f xs
                 | otherwise = filterL f xs

filterM :: Monad m => (a -> m Bool) -> [a] -> m [a]
filterM pred []     = return []
filterM pred (x:xs) = do b <- pred x
                         if b then do xsp <- mxsp
                                      return (x:xsp)
                              else mxsp
                              where mxsp = filterM pred xs

constN :: a -> b -> a
constN x _ = x

foo = filterM (constN [True,False])
test x = print $ foo x
